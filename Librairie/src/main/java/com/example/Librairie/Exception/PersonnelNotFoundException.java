package com.example.Librairie.Exception;

public class PersonnelNotFoundException extends RuntimeException {
    public PersonnelNotFoundException(String message){super(message);}
}
