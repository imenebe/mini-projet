package com.example.Librairie.Exception;

public class EtagereNotFoundException extends RuntimeException {
    public EtagereNotFoundException(String message){super(message);}
}