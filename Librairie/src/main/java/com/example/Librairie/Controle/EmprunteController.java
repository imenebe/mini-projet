package com.example.Librairie.Controle;

import com.example.Librairie.Modele.Emprunter;
import com.example.Librairie.Modele.Etagere;
import com.example.Librairie.Repository.EmprunterRepository;
import com.example.Librairie.Repository.EtagereRepository;
import com.example.Librairie.Service.EmprunteService;
import com.example.Librairie.Service.EtagereService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Emprunter")
@CrossOrigin(origins ="*")
public class EmprunteController {
    private final EmprunterRepository EP;
    private final EmprunteService service;


    public EmprunteController(EmprunterRepository EP, EmprunteService service) {

        this.EP = EP;
        this.service = service;
    }
    @GetMapping("/liste")
    public ResponseEntity<List<Emprunter>> getAllEmprunter() {
        List<Emprunter>E = service.findAllEmprunter();
        return new ResponseEntity<>(E, HttpStatus.OK);
    }
    @PostMapping("/add")
    public ResponseEntity<Emprunter>addEmprunter(@RequestBody Emprunter e) {
        Emprunter emp = service.addEmprunter(e);
        return new ResponseEntity<>(emp, HttpStatus.CREATED);
    }
}
