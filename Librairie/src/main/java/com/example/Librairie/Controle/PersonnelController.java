package com.example.Librairie.Controle;

import com.example.Librairie.Modele.Livre;
import com.example.Librairie.Modele.Personnels;
import com.example.Librairie.Repository.PersonnelRepository;
import com.example.Librairie.Service.PersonnelService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("Personnel")
public class PersonnelController {
    public final PersonnelRepository pers;
    public final PersonnelService ser;

    public PersonnelController(PersonnelRepository pers, PersonnelService ser) {
        this.pers = pers;
        this.ser = ser;
    }
    @GetMapping("/liste")
    public ResponseEntity<List<Personnels>> getAllPerson() {
        List<Personnels> per = ser.findAllPersonnels();
        return new ResponseEntity<>(per, HttpStatus.OK);
    }
    @GetMapping("/find/{idPersonnel}")
    public ResponseEntity<Personnels> getproduitByIdpersonnel(@PathVariable("idPersonnel") Long idPersonnel) {
        Personnels p= ser.RecherchePersonnel(idPersonnel);
        return new ResponseEntity<>(p, HttpStatus.OK);
    }



    @PostMapping("/addP")
    public ResponseEntity<Personnels> addPersonnel(@RequestBody Personnels Pers) {
        Personnels newper = ser.addPersonnel(Pers);
        return new ResponseEntity<>(newper, HttpStatus.CREATED);
    }
    @DeleteMapping("/delete/{idPersonnel}")
    public ResponseEntity<?> deletePersonnel(@PathVariable("idPersonnel") Long idPersonnel ) {
        ser.deletePersonnel(idPersonnel);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    @PutMapping("/update/{idPersonnel}")
    public ResponseEntity<Object> updatePersonnel(@RequestBody Personnels p ,@PathVariable("idPersonnel") Long idPersonnel ) {
        Optional<Personnels> PersonnelOptional = pers.findById(idPersonnel);
        if (PersonnelOptional.isEmpty())
            return ResponseEntity.notFound().build();
        p.setIdPersonnel(idPersonnel);
        pers.save(p);
        return ResponseEntity.noContent().build();
    }


}
