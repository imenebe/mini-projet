package com.example.Librairie.Controle;

import com.example.Librairie.Modele.Etagere;
import com.example.Librairie.Modele.Etudiant;
import com.example.Librairie.Repository.EtagereRepository;
import com.example.Librairie.Service.EtagereService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/Etagere")
@CrossOrigin(origins ="*")
public class EtagereController {
        private final EtagereRepository ER;
        private final EtagereService service;

        public EtagereController (EtagereService service, EtagereRepository repo) {
            super();
            this.service = service;
            this.ER = repo;
        }
    @GetMapping("/liste")
    public ResponseEntity<List<Etagere>> getAllEtagere() {
        List<Etagere> E = service.findAllEtagere();
        return new ResponseEntity<>(E, HttpStatus.OK);
    }
    @PostMapping("/add")
    public ResponseEntity<Etagere> addEtagere(@RequestBody Etagere e) {
        Etagere etag = service.addEtagere(e);
        return new ResponseEntity<>(etag, HttpStatus.CREATED);
    }
    @DeleteMapping("/delete/{idEtagere}")
    public ResponseEntity<?> deleteEtagere(@PathVariable("idEtagere") Long idEtagere ) {
        service.deleteEtagere(idEtagere);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    @PutMapping("/update/{idEtagere}")
    public ResponseEntity<Object> updateEtagere(@RequestBody Etagere etag ,@PathVariable("idEtagere") Long idEtagere ) {
        Optional<Etagere> EtagereOptional = ER.findById(idEtagere);
        if (EtagereOptional.isEmpty())
            return ResponseEntity.notFound().build();
        etag.setIdEtagere(idEtagere);
        ER.save(etag);
        return ResponseEntity.noContent().build();
    }

}
