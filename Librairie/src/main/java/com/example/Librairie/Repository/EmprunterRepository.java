package com.example.Librairie.Repository;

import com.example.Librairie.Modele.Emprunter;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmprunterRepository extends JpaRepository<Emprunter, Long> {
    Emprunter findEtagereByIdEmprunte(Long idEmprunte);
}



