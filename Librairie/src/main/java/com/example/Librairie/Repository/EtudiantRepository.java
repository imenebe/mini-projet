package com.example.Librairie.Repository;

import com.example.Librairie.Modele.Etudiant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EtudiantRepository extends JpaRepository<Etudiant, Long> {
    Etudiant findEtudiantByIdEtudiant(Long idEtudiant);
}

