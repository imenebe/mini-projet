package com.example.Librairie.Repository;

import com.example.Librairie.Modele.Etagere;
import com.example.Librairie.Modele.Etudiant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EtagereRepository extends JpaRepository<Etagere, Long> {
    Etudiant findEtagereByIdEtagere(Long idEtagere);
}

