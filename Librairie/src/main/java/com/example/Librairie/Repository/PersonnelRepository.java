package com.example.Librairie.Repository;

import com.example.Librairie.Modele.Personnels;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonnelRepository extends JpaRepository<Personnels,Long> {
   Personnels findPersonnelsByIdPersonnel(Long idPersonnel);
}
