package com.example.Librairie.Service;
import com.example.Librairie.Modele.Etagere;
import com.example.Librairie.Repository.EtagereRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class EtagereService {
    private final EtagereRepository ER;
    public EtagereService(EtagereRepository e) {
        this.ER = e;
    }
    public Etagere addEtagere(Etagere e) {
        return ER.save(e);
    }
    public List<Etagere> findAllEtagere() {
        return ER.findAll();
    }

    public void deleteEtagere(Long idEtagere)
    {
        ER.deleteById(idEtagere);
    }

}
