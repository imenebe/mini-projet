package com.example.Librairie.Service;

import com.example.Librairie.Exception.EtudiantNotFoundException;
import com.example.Librairie.Modele.Etudiant;
import com.example.Librairie.Repository.EtudiantRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class EtudiantService {
    private final EtudiantRepository etu;
    public EtudiantService(EtudiantRepository et) {
        this.etu = et;
    }
    public Etudiant addEtudiant(Etudiant prod) {
        return etu.save(prod);
    }

    public List<Etudiant> findAllEtudiants() {
        return etu.findAll();
    }

    public Etudiant findEtudiantByIdEtudiant(Long idEtudiant) {
        return etu.findById(idEtudiant).orElseThrow(() -> new EtudiantNotFoundException(
                "Produit by id " + idEtudiant + " was not found"));
    }

    public void deleteEtudiant(Long idEtudiant){etu.deleteById(idEtudiant);
    }




}

