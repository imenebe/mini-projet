package com.example.Librairie.Service;

import com.example.Librairie.Modele.Emprunter;
import com.example.Librairie.Modele.Personnels;
import com.example.Librairie.Repository.EmprunterRepository;
import com.example.Librairie.Repository.EtudiantRepository;
import com.example.Librairie.Repository.PersonnelRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class EmprunteService {
    private final EmprunterRepository emp;
    public EmprunteService(EmprunterRepository empo) {
        this.emp = empo;}
    public List<Emprunter> findAllEmprunter() {
        return emp.findAll();
    }
    public Emprunter addEmprunter(Emprunter p) {return emp.save(p);}

}
