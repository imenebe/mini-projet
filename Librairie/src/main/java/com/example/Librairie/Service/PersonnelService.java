package com.example.Librairie.Service;

import com.example.Librairie.Exception.LivreNotFoundException;
import com.example.Librairie.Exception.PersonnelNotFoundException;
import com.example.Librairie.Modele.Livre;
import com.example.Librairie.Modele.Personnels;
import com.example.Librairie.Repository.PersonnelRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class PersonnelService {
public final PersonnelRepository perso;

    public PersonnelService(PersonnelRepository pro) {
        this.perso = pro;}
    public List<Personnels> findAllPersonnels() {
        return perso.findAll();
    }
    public Personnels addPersonnel(Personnels p) {return perso.save(p);}

    public Personnels RecherchePersonnel(Long idPersonnel) {
        return perso.findById(idPersonnel).orElseThrow(() -> new PersonnelNotFoundException(
                "le personne by id " + idPersonnel + " was not found"));
    }

    public void deletePersonnel(Long idPersonnel){perso.deleteById(idPersonnel);
    }
}
