package com.example.Librairie.Modele;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Livre {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idLivre;
    private String titre;
    private String description;
    private int nbpage;
    @JoinColumn(name = "idPersonnels")
    @ManyToOne(cascade = {CascadeType.MERGE})
    private  Personnels  Personne;


    @JoinColumn(name = "idEtagere")
    @ManyToOne(cascade = {CascadeType.MERGE})
    private  Etagere  etagere;


    @OneToMany(mappedBy = "Liv",cascade = {CascadeType.MERGE})
    private Set<Emprunter> E;


    public Livre() {

    }

    public Livre(String titre, String description, int nbpage, Personnels personne, Etagere etagere) {
        this.titre = titre;
        this.description = description;
        this.nbpage = nbpage;
        Personne = personne;
        this.etagere = etagere;
    }

    public Livre(String titre, String description, int nbpage) {
        this.titre = titre;
        this.description = description;
        this.nbpage = nbpage;
    }

    public Long getIdLivre() {
        return idLivre;
    }

    public void setIdLivre(Long idLivre) {
        this.idLivre = idLivre;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNbpage() {
        return nbpage;
    }

    public void setNbpage(int nbpage) {
        this.nbpage = nbpage;
    }

    public Personnels getPersonne() {
        return Personne;
    }

    public void setPersonne(Personnels personne) {
        Personne = personne;
    }

    public Etagere getEtagere() {
        return etagere;
    }

    public void setEtagere(Etagere etagere) {
        this.etagere = etagere;
    }

    @Override
    public String toString() {
        return "Livre{" +
                "idLivre=" + idLivre +
                ", titre='" + titre + '\'' +
                ", description='" + description + '\'' +
                ", nbpage=" + nbpage +
                ", Personne=" + Personne +
                ", etagere=" + etagere +
                '}';
    }
}
