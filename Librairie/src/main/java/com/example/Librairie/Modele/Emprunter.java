package com.example.Librairie.Modele;

import javax.persistence.*;
import java.util.Date;

@Entity

public class Emprunter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long idEmprunte ;
    private Date date;
    @JoinColumn(name = "idLivre")
    @ManyToOne(cascade = {CascadeType.MERGE})
    private  Livre Liv;

   // @JoinColumn(name = "idEtudiant")
    @ManyToOne(cascade = {CascadeType.MERGE})
    private Etudiant  Etud;


    public Emprunter() {
    }

    public Long getIdEmprunte() {
        return idEmprunte;
    }

    public void setIdEmprunte(Long idEmprunte) {
        this.idEmprunte = idEmprunte;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Livre getLiv() {
        return Liv;
    }

    public void setLiv(Livre liv) {
        Liv = liv;
    }

    public Etudiant getEtud() {
        return Etud;
    }

    public void setEtud(Etudiant etud) {
        Etud = etud;
    }

    @Override
    public String toString() {
        return "Emprunter{" +
                "idEmprunte=" + idEmprunte +
                ", date=" + date +
                ", Liv=" + Liv +
                ", Etud=" + Etud +
                '}';
    }
}
