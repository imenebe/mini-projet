package com.example.Librairie.Modele;

import javax.persistence.*;
import java.util.Set;


@Entity
public class Personnels {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 5)
    private Long idPersonnel;
    private String codeP;
    private String nom;
    private String prenom;

    public Personnels(String codeP, String nom, String prenom) {
        this.codeP = codeP;
        this.nom = nom;
        this.prenom = prenom;
    }

    public Personnels() {

    }
    @OneToMany(mappedBy = "Personne",cascade = {CascadeType.MERGE})
    private Set<Livre> L;

    public Long getIdPersonnel() {
        return idPersonnel;
    }

    public void setIdPersonnel(Long idPersonnel) {
        this.idPersonnel = idPersonnel;
    }

    public String getCodeP() {
        return codeP;
    }

    public void setCodeP(String codeP) {
        this.codeP = codeP;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Override
    public String toString() {
        return "Personnels{" +
                "idPersonnel=" + idPersonnel +
                ", codeP='" + codeP + '\'' +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                '}';
    }
}
