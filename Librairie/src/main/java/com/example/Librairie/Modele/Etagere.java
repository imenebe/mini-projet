package com.example.Librairie.Modele;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Etagere {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idEtagere;
    private String codeE;
    private String titre;
    private int nb_liver;


    @OneToMany(mappedBy = "etagere",cascade = {CascadeType.MERGE})
    private Set<Livre> L;

    public Etagere(String codeE, String titre, int nb_liver) {
        this.codeE = codeE;
        this.titre = titre;
        this.nb_liver = nb_liver;
    }

    public Etagere() {
    }

    public Long getIdEtagere() {
        return idEtagere;
    }

    public void setIdEtagere(Long idEtagere) {
        this.idEtagere = idEtagere;
    }

    public String getCodeE() {
        return codeE;
    }

    public void setCodeE(String codeE) {
        this.codeE = codeE;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getNb_liver() {
        return nb_liver;
    }

    public void setNb_liver(int nb_liver) {
        this.nb_liver = nb_liver;
    }

    @Override
    public String toString() {
        return "Etagere{" +
                "idEtagere=" + idEtagere +
                ", codeE='" + codeE + '\'' +
                ", titre='" + titre + '\'' +
                ", nb_liver=" + nb_liver +
                '}';
    }
}
