package com.example.Librairie.Modele;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Etudiant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long idEtudiant;
    private int CIN;
    private String nom;
    private String prenom;
    private String classe;


    @OneToMany(mappedBy = "Etud",cascade = {CascadeType.MERGE})
    private Set<Emprunter> E;

    public Etudiant( String nom, String prenom, String classe, int CIN) {
        this.nom = nom;
        this.prenom = prenom;
        this.classe = classe;
        this.CIN = CIN;
    }


    public Etudiant(){
    }

    public Long getIdEtudiant() {
        return idEtudiant;
    }

    public void setIdEtudiant(Long idEtudiant) {
        this.idEtudiant = idEtudiant;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    public int getCIN() {
        return CIN;
    }

    public void setCIN(int CIN) {
        this.CIN = CIN;
    }



    @Override
    public String toString() {
        return "Etudiant{" +
                "idEtudiant=" + idEtudiant +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", classe='" + classe + '\'' +
                ", CIN=" + CIN +
                '}';
    }
}
